# Use Gitlab's OpenTofu base image
FROM registry.gitlab.com/components/opentofu/gitlab-opentofu:0.10.0-opentofu1.6.1 as base

# Install 1Password CLI
COPY --from=1password/op:2 /usr/local/bin/op /usr/local/bin/op

# Install kubectl 
RUN apk add kubectl
